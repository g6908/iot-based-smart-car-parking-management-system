import time
import grovepi


pir_sensor = 2
motion=0
grovepi.pinMode(pir_sensor,"INPUT")

while True:
    

    motion=grovepi.digitalRead(pir_sensor)
    if motion==0 or motion==1:	# check if reads were 0 or 1 it can be 255 also because of IO Errors so remove those values
        if motion==1:
            print ('Motion Detected')
        else:
            print ('-')

# if your hold time is less than this, you might not see as many detections
    time.sleep(.2)


import video_capture_Q_buf as vid_cap_q # import as alias
from video_capture_Q_buf import VideoCaptureQ # class import
import time
from detector import *
import cv2

cap = VideoCaptureQ(0)
car_cascade = cv2.CascadeClassifier('carss.xml')

while True:

    t1 = time.time()

    if vid_cap_q.is_frame == False:
        print('no more frames left')
        break

    try:
        carlist = Simulator(cap.cap)
        return_rects(carlist)
    except Exception as e:
        print(e)
        break
    t2 = time.time()
    print(f'FPS: {1/(t2-t1)}')
                    
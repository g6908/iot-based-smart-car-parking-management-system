import cv2
import numpy as np
import statistics
from picamera import PiCamera
from picamera.array import PiRGBArray
import time
import grovepi
from operator import itemgetter
from display import inputText
from cloud import takeInput


def return_rects(imagep):
    img = imagep
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gray = cv2.bilateralFilter(gray, 11, 17, 17)

    kernel = np.ones((5,5),np.uint8)
    erosion = cv2.erode(gray,kernel,iterations = 2)
    kernel = np.ones((4,4),np.uint8)
    dilation = cv2.dilate(erosion,kernel,iterations = 2)

    edged = cv2.Canny(dilation, 30, 200)

    img5,contours, hierarchy = cv2.findContours(edged, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    rects = [cv2.boundingRect(cnt) for cnt in contours]
    rects = sorted(rects,key=lambda  x:x[1],reverse=True)
    #print(rects)

    newlis = []
    for rect_init in rects:
        x,y,w,h = rect_init
        area = w * h
        newlis.append(area)
        
    rectlist = []
    past = max([p[0] for p in statistics._counts(newlis)])
    rectsofi = []
    for rect in rects:
        x,y,w,h = rect
        area = w * h
        #if past == 0:
        #    past = area
        #if past < area+20000 and past > area-20000:
        #    past = area
        rectsofi.append(rect)
            
            #cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),3)
            

    return rectsofi

def returnMotion(count):
    pir_sensor = 2
    motion=0
    grovepi.pinMode(pir_sensor,"INPUT")

    motion=grovepi.digitalRead(pir_sensor)
    if motion==0 or motion==1:	# check if reads were 0 or 1 it can be 255 also because of IO Errors so remove those values
        if motion==1:
            print ('Car Detected')
            count = 10

                
    return count

def unique(arr):
    
    unique = []
    dictxy = dict()

    
    for x in arr:
        if x not in unique and 70< x[2] < 100 and 100 < x[3] < 120 :
            unique.append(x)
            dictxy[x] = (x[0],round(x[1],-2))
            
            
    y1 = 0
    y2 = 0
    for key,value in dictxy.items():
        #print(value[1])
        if value[1] != y1:
            y1 = value[1]
    
    for key,value in dictxy.items():
        if value[1] != y1:
            y2 = value[1]
            
    row1 = []
    row2 = []
    
    for key,value in dictxy.items():
        if value[1] == y1:
            row1.append(key)
        else:
            row2.append(key)
            
    
    row1 = (sorted(row1, key=itemgetter(0)))
    row2 = (sorted(row2, key=itemgetter(0)))
    unique = row1 + row2

    return unique
                
def buildStr(arr):
    v = []
    for n in range(len(arr)):
        if arr[n] == 0:
            v.append(n+1)
        
    strin = "Bays Open:\n"
    for val in v:
        strin += (str(val) + ' ')
    return strin

def Simulator(cap):
    number_of_frames = int(cap.get(cv2.CAP_PROP_FPS))
    count = 0
    count1 = 0
    votesc = 0
    newlis = []
    votes = [0,0,0,0,0,0,0,0]
    while(cap.isOpened() or count1 == 0):
        ret, frames = cap.read()
        if count == 0:
            imagecap = frames
            rectsc = unique(return_rects(imagecap))
            #print((rectsc))
            #print((unique(rectsc)))
            for rect_init in rectsc:
                x,y,w,h = rect_init
                area = w * h
                area = round(area,-3)
                newlis.append(area)
            past = max([p[0] for p in statistics._counts(newlis)])
            #print(newlis)
            count += 1
        
        
        #print((unique(rectsc)))
        gray = cv2.cvtColor(frames, cv2.COLOR_BGR2GRAY)
        cars = car_cascade.detectMultiScale(frames,1.003,6,minSize=(30,30),maxSize=(80,110))
        count1 = returnMotion(count1)
        
        arr = [0,0,0,0,0,0,0,0]
        
        v = 19
        if ret and count1 != 0:
            count1 -= 1
            if votesc >= 10:
                votesc = 0
                votes = [0,0,0,0,0,0,0,0]
            carlist = []
            for (x,y,w,h) in cars:
                carlist.append((x,y,w,h))
                cv2.rectangle(frames,(x,y),(x+w,y+h),(0,255,0),2)
           
            for r in range(len(rectsc)):
                rect = rectsc[r]
                x,y,w,h = rect
                past = 7000 
                area = w*h
                if past == 0:
                    past = area

                if past < area+3500 and past > area-3500 and area > 8000:
                    past = area
                    #print(area)
                    cv2.rectangle(frames, (x,y),(x+w,y+h), (255,0,0),2)
                    arr[r] = 0
                    for (x1,y1,w1,h1) in cars:
                        if x1+(w1//2) > x and x1+(w1//2) < x+w and y1+(h1//2) > y and y1+(h1//2) < y+h:
                            cv2.rectangle(frames, (x,y),(x+w,y+h), (0,0,255),2)
                            #print(r)
                            votes[r] += 1
                            if votes[r] > 8:
                                arr[r] = 1
                            
                v -= 1
            
            #print(arr)
            inputText(buildStr(arr))
            m = ["Parking Space 1","Parking Space 2","Parking Space 3","Parking Space 4","Parking Space 5","Parking Space 6","Parking Space 7","Parking Space 8"]
            new = {"Parking Space 1":0,"Parking Space 2":0,"Parking Space 3":0,"Parking Space 4":0,"Parking Space 5":0,"Parking Space 6":0,"Parking Space 7":0,"Parking Space 8":0}
            for n in range(len(m)):
                new[m[n]] = arr[n]
            #print(new)
            takeInput(new)
            cv2.imshow('video', frames)

            if cv2.waitKey(25) & 0xFF == ord('q'):
                break
        else:
            cv2.imshow('video', frames)
            m = ["Parking Space 1","Parking Space 2","Parking Space 3","Parking Space 4","Parking Space 5","Parking Space 6","Parking Space 7","Parking Space 8"]
            new = {"Parking Space 1":0,"Parking Space 2":0,"Parking Space 3":0,"Parking Space 4":0,"Parking Space 5":0,"Parking Space 6":0,"Parking Space 7":0,"Parking Space 8":0}
            for n in range(len(m)):
                new[m[n]] = arr[n]
            takeInput(new)
            if cv2.waitKey(25) & 0xFF == ord('q'):
                break
        
print("ddd")    
car_cascade = cv2.CascadeClassifier('testcars.xml')
cap1 = cv2.VideoCapture(0)
carlist = Simulator(cap1)

cap1.release()
cv2.destroyAllWindows()



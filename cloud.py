import os
import time
import paho.mqtt.client as mqtt
import json
import grovepi

# THINGSBOARD_HOST = 'demo.thingsboard.io'
THINGSBOARD_HOST = 'thingsboard.cs.cf.ac.uk'
ACCESS_TOKEN = 'w6VsaRWj2qQYxMACqlaE' # <== Insert your own access token here.

# Check if we can successfully push a data to thingsboard
def on_publish(client,userdata,result):
    print("Success")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print ('Topic: ' + msg.topic + '\nMessage: ' + str(msg.payload))
    # Decode JSON request
    data = json.loads(msg.payload)
    #print(data)
    # Check request method
    if data['method'] == 'setValue':
        print(data['params']) 
        buzzer_state['State'] = data['params']
        grovepi.digitalWrite(buzzer, data['params'])
    
# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, rc, *extra_params):
    print('Connected with result code ' + str(rc))

        # Data capture and upload interval in seconds. Less interval will eventually hang the DHT11.
INTERVAL=3

client = mqtt.Client()
    # Set access token
client.username_pw_set(ACCESS_TOKEN)
    # Connect to ThingsBoard using default MQTT port and 60 seconds keepalive interval
client.connect(THINGSBOARD_HOST, 1883, 60)
    # Register connect callback
client.on_connect = on_connect
    # Register publish callback
client.on_publish = on_publish
    # Registed publish message callback
client.on_message = on_message



def takeInput(data):

    # subscribe to RPC commands from the server - This will let you to control your buzzer.
    client.subscribe('v1/devices/me/rpc/request/+')

    client.loop_start()
    try:
        client.publish('v1/devices/me/telemetry', json.dumps(data), retain=False)
        client.loop_stop()

    except KeyboardInterrupt:
        client.loop_stop()
        client.disconnect()
        print ("Terminated.")
        os._exit(0)
